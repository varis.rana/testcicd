import { Test, TestingModule } from "@nestjs/testing";

describe("UserServicesController", () => {
  describe("root", () => {
    it('should return "Hello World!"', () => {
      expect("Hello World!").toBe("Hello World!");
    });
  });
});
